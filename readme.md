# Baza informacji o miejscach z jedzeniem w okolicy naszego biura w Business Garden

Dane o miejscach z jedzeniem znajdują się w pliku `data\food-points.json`.
Jeśli chcesz dodać nową miejscówkę, edytuj ww. plik i stwórz merge requesta. Po weryfikacji zmiany i jej akceptacji, zostanie ona scalona z głównym repozytorium.